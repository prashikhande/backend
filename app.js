var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
const mongo = require('mongoskin');
const config = require('./config');
const socket = require('socket.io')

var index = require('./routes/index');

var app = express();

// Socket.io
var io = socket();
app.io = io;

// This is what the socket.io syntax is like, we will work this later
io.on('connection', function (socket) {
  console.log('User connected');

  socket.on('disconnect', function () {
    console.log('user disconnected')
  })
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/*
 * Database Connection
 * */

const db = mongo.db(config.mongoUrl, {native_parser: true});
app.use(function (req, res, next) {
  req.db = db;
  req.io = io;
  next();
});


app.use('/', index);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;
