var express = require('express');
var router = express.Router();
const ObjectId = require("mongodb").ObjectID;
const excelbuilder = require('msexcel-builder');
const csv = require('fast-csv');
const fs = require('fs')


/**
 * Send updated data to all connected clients
 * @param req
 */
function brodcast(req) {
  req.db.collection('Orders').find({Quantity: {$gt: 0}}).toArray(function (err, data) {
    if (err) {
      //nothing to do
    } else {
      req.io.emit('update', data);
    }
  });
}

/**
 * Default route
 */
router.get('/', function (req, res, next) {
  res.render('index', {title: 'Express'});
});

/**
 * Route for adding the order
 */
router.post('/addOrder', function (req, res) {
  var db = req.db;
  var dish = req.body.dish;
  var quantity = req.body.quantity;
  var whenAdded = new Date(req.body.whenAdded);


  if (!(dish && quantity)) {
    console.log("Bad request");
    return res.status(400).json({
      status: 400,
      message: 'Bad request'
    });
  }

  db.collection('Orders').findOne({Dish: dish}, function (err, data) {
    if (err) {
      res.status(500).json({
        status: 500,
        message: 'Server error'
      });
    }
    else if (!data) {
      db.collection('Orders').insert({
        Dish: dish,
        Quantity: parseInt(quantity),
        Created: 0,
        Predicted: 10,
        WhenAdded: whenAdded,
      }, function (err, data) {
        if (err) {
          res.status(500).json({
            status: 500,
            message: 'Server error'
          });
        } else {
          brodcast(req);
          res.status(200).json({
            status: 200,
            message: 'Success'
          });
        }
      });
    }
    else {
      // console.log(data);
      db.collection('Orders').update({Dish: dish}, {
        $set: {
          Quantity: data.Quantity + parseInt(quantity),
        }
      }, function (err, data) {
        if (err) {
          res.status(500).json({
            status: 500,
            message: 'Server error'
          });
        } else {
          brodcast(req);
          res.status(200).json({
            status: 200,
            message: 'Success'
          });
        }
      })
    }
  });
});

/**
 * Route for getting all orders non-zero(>0) quantity
 */
router.get('/getOrders', function (req, res, next) {
  var db = req.db;
  db.collection('Orders').find({Quantity: {$gt: 0}}).toArray(function (err, data) {
    if (err) {
      res.status(500).json({
        status: 500,
        message: 'Server error'
      });
    } else {
      res.status(200).json({
        status: 200,
        message: 'Success',
        data: data
      });
    }
  });
});


/**
 * Update the existing order
 */
router.post('/updateOrder', function (req, res) {
  var db = req.db;
  var id = ObjectId(req.body.id);
  db.collection('Orders').findOne({_id: id}, function (err, data) {
    if (err) {
      res.status(500).json({
        status: 500,
        message: 'Server error'
      });
    }
    else {
      // console.log(data);
      db.collection('Orders').update({_id: id}, {
        $set: {
          Quantity:0,
          Created: data.Created + data.Quantity,
        }
      }, function (err, data) {
        if (err) {
          res.status(500).json({
            status: 500,
            message: 'Server error'
          });
        } else {
          brodcast(req);
          res.status(200).json({
            status: 200,
            message: 'Success'
          });
        }
      })
    }
  });
});


/**
 * Download data in excel format
 */
router.get('/download', function (req, res) {
  req.db.collection('Orders').find({}).toArray(function (err, data) {
    if (err) {
      console.log(err);
    } else {
      const noOfColumns = 3;

      var ws = fs.createWriteStream('./public/Orders.csv');
      const columnHeaders = ['Dish Name', 'Produced', 'Predicted'];

      var sheetDataArray  =[];
      sheetDataArray.push(columnHeaders);


      for (var i = 0; i < data.length; i++) {
        var row = [data[i].Dish,data[i].Created,data[i].Predicted]
        sheetDataArray.push(row);
      }

      csv.write(sheetDataArray,{headers:true})
          .pipe(ws);
      res.status(200).json({
        status: 200,
        message: 'Success'
      });
    }
  });
});

/**
 * Setting predicted value for given Dish
 */
router.post('/setPredicted', function (req, res) {
  var db = req.db;
  var dish = req.body.dish;
  var predicted = parseInt(req.body.predicted);

  if (!(dish && !isNaN(predicted) && predicted>0)) {
    console.log("Bad request");
    return res.status(400).json({
      status: 400,
      message: 'Bad request'
    });
  }

  db.collection('Orders').findOne({Dish: req.body.dish}, function (err, data) {
    if (err) {
      res.status(500).json({
        status: 500,
        message: 'Server error'
      });
    }
    else if (!data) {
      res.status(400).json({
        status: 400,
        message: 'Dish not found'
      });
    }
    else {
      // console.log(data);
      db.collection('Orders').update({Dish: req.body.dish}, {
        $set: {
          Predicted: parseInt(req.body.predicted),
        }
      }, function (err, data) {
        if (err) {
          res.status(500).json({
            status: 500,
            message: 'Server error'
          });
        } else {
          brodcast(req);
          res.status(200).json({
            status: 200,
            message: 'Success'
          });
        }
      })
    }
  });
});

module.exports = router;
